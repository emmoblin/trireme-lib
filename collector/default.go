package collector

import (
	"encoding/binary"
	"fmt"
	client "github.com/influxdata/influxdb/client/v2"
	"strconv"

	"go.uber.org/zap"
	"sort"
	"strings"
	"time"

	"github.com/cespare/xxhash"
)

// DefaultCollector implements a default collector infrastructure to syslog
type DefaultCollector struct{}

// NewDefaultCollector returns a default implementation of an EventCollector
func NewDefaultCollector() EventCollector {
	zap.L().Info("Using default empty collector")
	return &DefaultCollector{}
}

// NewInfluxDBCollector returns a collector implementation for InfluxDB
func NewInfluxDBCollector(user, pass, url, db string, insecureSkipVerify bool) EventCollector {
	zap.L().Info("Using Influx collector", zap.String("endpoint", url), zap.String("user", user))
	collectorInstance, err := NewDBConnection(user, pass, url, db, insecureSkipVerify)
	if err != nil {
		zap.L().Error("Error instantiating Influx collector, using default", zap.String("endpoint", url), zap.String("user", user), zap.Error(err))
		return NewDefaultCollector()
	}
	collectorInstance.Start()
	return collectorInstance
}

// CollectFlowEvent is part of the EventCollector interface.
func (d *DefaultCollector) CollectFlowEvent(record *FlowRecord) {}

// CollectContainerEvent is part of the EventCollector interface.
func (d *DefaultCollector) CollectContainerEvent(record *ContainerRecord) {}

// CollectUserEvent is part of the EventCollector interface.
func (d *DefaultCollector) CollectUserEvent(record *UserRecord) {}

// CollectTraceEvent collects iptables trace events
func (d *DefaultCollector) CollectTraceEvent(records []string) {}

// CollectPacketEvent collects packet events from the datapath
func (d *DefaultCollector) CollectPacketEvent(report *PacketReport) {}

// CollectCounterEvent collect counters from the datapath
func (d *DefaultCollector) CollectCounterEvent(report *CounterReport) {}

// CollectDNSRequests collect counters from the datapath
func (d *DefaultCollector) CollectDNSRequests(report *DNSRequestReport) {}

// StatsFlowHash is a hash function to hash flows
func StatsFlowHash(r *FlowRecord) string {
	hash := xxhash.New()
	hash.Write([]byte(r.Source.ID))      // nolint errcheck
	hash.Write([]byte(r.Destination.ID)) // nolint errcheck
	port := make([]byte, 2)
	binary.BigEndian.PutUint16(port, r.Destination.Port)
	hash.Write(port)                              // nolint errcheck
	hash.Write([]byte(r.Action.String()))         // nolint errcheck
	hash.Write([]byte(r.ObservedAction.String())) // nolint errcheck
	hash.Write([]byte(r.DropReason))              // nolint errcheck
	hash.Write([]byte(r.Destination.URI))         // nolint errcheck

	return fmt.Sprintf("%d", hash.Sum64())
}

// StatsUserHash is a hash function to hash user records.
func StatsUserHash(r *UserRecord) error {
	// Order matters for the hash function loop
	sort.Strings(r.Claims)
	hash := xxhash.New()
	for i := 0; i < len(r.Claims); i++ {
		if strings.HasPrefix(r.Claims[i], "sub") {
			continue
		}
		if _, err := hash.Write([]byte(r.Claims[i])); err != nil {
			return fmt.Errorf("unable to create hash: %v", err)
		}
	}

	hashWithNS, err := HashHashWithNamespace(fmt.Sprintf("%d", hash.Sum64()), r.Namespace)
	if err != nil {
		return err
	}

	r.ID = hashWithNS

	return nil
}

// HashHashWithNamespace hash the given claim hash with the given namespace.
func HashHashWithNamespace(claimsHash string, namespace string) (string, error) {

	hash := xxhash.New()
	if _, err := hash.Write(append([]byte(claimsHash), []byte(namespace)...)); err != nil {
		return "", fmt.Errorf("unable to create namespace hash: %v", err)
	}

	return fmt.Sprintf("%d", hash.Sum64()), nil
}

//Influxdb inplements a DataAdder interface for influxDB
type Influxdb struct {
	httpClient client.Client
	database   string

	stopWorker chan struct{}
	worker     *worker
}

//DataAdder interface has all the methods required to interact with influxdb api
type DataAdder interface {
	CreateDB(string) error
	AddData(tags map[string]string, fields map[string]interface{}) error
	ExecuteQuery(query string, dbname string) (*client.Response, error)
}

// NewDBConnection is used to create a new client and return influxdb handle
func NewDBConnection(user string, pass string, addr string, db string, insecureSkipVerify bool) (*Influxdb, error) {
	zap.L().Debug("Initializing InfluxDBConnection")
	httpClient, err := createHTTPClient(user, pass, addr, insecureSkipVerify)
	if err != nil {
		return nil, fmt.Errorf("Error parsing url %s", err)
	}
	_, _, err = httpClient.Ping(time.Second * 0)
	if err != nil {
		return nil, fmt.Errorf("Unable to create InfluxDB http client %s", err)
	}

	dbConnection := &Influxdb{
		httpClient: httpClient,
		database:   db,
		stopWorker: make(chan struct{}),
	}

	worker := newWorker(dbConnection.stopWorker, dbConnection)
	dbConnection.worker = worker

	// Attempt to create the Database. Silently fail if it already exists.
	if err := dbConnection.CreateDB(db); err != nil {
		return nil, fmt.Errorf("Error: Creating Database: %s", err)
	}

	return dbConnection, nil
}

func createHTTPClient(user string, pass string, addr string, InsecureSkipVerify bool) (client.Client, error) {

	// TODO: Make the timeout configurable
	httpClient, err := client.NewHTTPClient(client.HTTPConfig{
		Addr:               addr,
		Username:           user,
		Password:           pass,
		Timeout:            20 * time.Second,
		InsecureSkipVerify: InsecureSkipVerify,
	})
	if err != nil {
		return nil, err
	}

	return httpClient, nil
}

// CreateDB is used to create a new databases given name
func (d *Influxdb) CreateDB(dbname string) error {
	zap.L().Info("Creating database", zap.String("db", dbname))

	_, err := d.ExecuteQuery("CREATE DATABASE "+dbname, "")
	if err != nil {
		return err
	}

	return nil
}

// ExecuteQuery is used to execute a query given a database name
func (d *Influxdb) ExecuteQuery(query string, dbname string) (*client.Response, error) {

	q := client.Query{
		Command:  query,
		Database: dbname,
		Chunked:  false,
	}

	response, err := d.httpClient.Query(q)
	if err != nil {
		return nil, err
	}

	return response, nil
}

// Start is used to start listening for data
func (d *Influxdb) Start() error {
	zap.L().Info("Starting InfluxDB worker")

	go d.worker.startWorker()

	return nil
}

// Stop is used to stop and return from listen goroutine
func (d *Influxdb) Stop() error {
	zap.L().Info("Stopping InfluxDB worker")

	d.stopWorker <- struct{}{}
	d.httpClient.Close()

	return nil
}

// AddData is used to add data to the batch
func (d *Influxdb) AddData(tags map[string]string, fields map[string]interface{}) error {
	zap.L().Debug("Calling AddData", zap.Any("tags", tags), zap.Any("fields", fields))
	bp, err := client.NewBatchPoints(client.BatchPointsConfig{
		Database:  d.database,
		Precision: "us",
	})
	if err != nil {
		return fmt.Errorf("Couldn't add data, error creating batchpoint: %s", err)
	}

	if tags[EventName] == EventTypeContainerStart || tags[EventName] == EventTypeContainerStop {
		pt, err := client.NewPoint(EventTypeContainer, tags, fields, time.Now())
		if err != nil {
			return fmt.Errorf("Couldn't add ContainerEvent: %s", err)
		}
		bp.AddPoint(pt)
	} else if tags[EventName] == EventTypeFlow {
		pt, err := client.NewPoint(EventTypeFlow, tags, fields, time.Now())
		if err != nil {
			return fmt.Errorf("Couldn't add FlowEvent: %s", err)
		}
		bp.AddPoint(pt)
	}
	if err := d.httpClient.Write(bp); err != nil {
		return fmt.Errorf("Couldn't add data: %s", err)
	}

	return nil
}

// CollectFlowEvent implements trireme collector interface
func (d *Influxdb) CollectFlowEvent(record *FlowRecord) {
	d.worker.addEvent(
		&workerEvent{
			event:      flowEvent,
			flowRecord: record,
		},
	)
}

// CollectContainerEvent implements trireme collector interface
func (d *Influxdb) CollectContainerEvent(record *ContainerRecord) {
	d.worker.addEvent(
		&workerEvent{
			event:           containerEvent,
			containerRecord: record,
		},
	)
}

// CollectUserEvent implements trireme collector interface
func (d *Influxdb) CollectUserEvent(record *UserRecord) {
	// TODO: Implement this event correctly
	zap.L().Debug("CollectUserEvent not yet implemented in Trireme-Statistics")
}

// CollectTraceEvent collects a set of trace messages generated with Iptables trace command
func (d *Influxdb) CollectTraceEvent(records []string) {}

// CollectPacketEvent collects packet event from nfqdatapath
func (d *Influxdb) CollectPacketEvent(report *PacketReport) {}

// CollectCounterEvent collects the counters from
func (d *Influxdb) CollectCounterEvent(counterReport *CounterReport) {}

// CollectDNSRequests collects the dns requests
func (d *Influxdb) CollectDNSRequests(request *DNSRequestReport) {}

const (
	// EventName is the constant used to store the name of the event type
	EventName = "EventName"

	// EventTypeFlow is the constant used to store event of type flows
	EventTypeFlow = "FlowEvents"

	// EventTypeContainer is the constant used to store event of type container
	EventTypeContainer = "ContainerEvents"

	// EventTypeContainerStart is the constant used to store event of type container start
	EventTypeContainerStart = "ContainerStartEvents"

	// EventTypeContainerStop is the constant used to store event of type container stop
	EventTypeContainerStop = "ContainerStopEvents"
)

// A worker manages the workload for the InfluxDB collector
type worker struct {
	events chan *workerEvent
	stop   chan struct{}
	db     DataAdder
}

type eventType int

const (
	containerEvent eventType = iota
	flowEvent      eventType = iota
)

// a workerEvent is an event that the worker need to process
type workerEvent struct {
	event           eventType
	containerRecord *ContainerRecord
	flowRecord      *FlowRecord
}

func newWorker(stop chan struct{}, db DataAdder) *worker {
	return &worker{
		events: make(chan *workerEvent, 500),
		stop:   stop,
		db:     db,
	}
}

func (w *worker) addEvent(wevent *workerEvent) {
	select {
	case w.events <- wevent: // Put event in channel unless it is full
		zap.L().Debug("Adding event to InfluxDBProcessingQueue.")
	default:
		zap.L().Warn("Event queue full for InfluxDB. Dropping event.")
	}
}

// startWorker start processing the event for this worker.
// Blocking... Use go.
func (w *worker) startWorker() {
	zap.L().Info("Starting InfluxDBworker")
	for {
		select {
		case event := <-w.events:
			w.processEvent(event)
		case <-w.stop:
			return
		}
	}
}

func (w *worker) processEvent(wevent *workerEvent) {
	zap.L().Debug("Processing event for InfluxDB")

	switch wevent.event {
	case containerEvent:
		if err := w.doCollectContainerEvent(wevent.containerRecord); err != nil {
			zap.L().Error("Couldn't process influxDB Request ContainerRequest", zap.Error(err))
		}

	case flowEvent:
		if err := w.doCollectFlowEvent(wevent.flowRecord); err != nil {
			zap.L().Error("Couldn't process influxDB Request FlowRequest", zap.Error(err))
		}
	}
}

// CollectContainerEvent implements trireme collector interface
func (w *worker) doCollectContainerEvent(record *ContainerRecord) error {
	var eventName string

	switch record.Event {
	case ContainerStart, ContainerUpdate, ContainerCreate:
		eventName = EventTypeContainerStart
	case ContainerDelete, ContainerStop:
		eventName = EventTypeContainerStop
	case ContainerIgnored:
		// Used for non relevant container events.
		return nil
	case ContainerFailed:
		// TODO: handle ContainerFailed event type
		return nil
	default:
		return fmt.Errorf("Unrecognized container event name %s ", record.Event)
	}
	var IPAddress string
	for _, v := range record.IPAddress {
		IPAddress = v
	}
	return w.db.AddData(map[string]string{
		"EventName": eventName,
		"EventID":   record.ContextID,
	}, map[string]interface{}{
		"ContextID": record.ContextID,
		"IPAddress": IPAddress,
		"Tags":      record.Tags,
		"Event":     record.Event,
	})
}

// CollectFlowEvent implements trireme collector interface
func (w *worker) doCollectFlowEvent(record *FlowRecord) error {
	return w.db.AddData(map[string]string{
		"EventName":       EventTypeFlow,
		"EventID":         record.ContextID,
		"SourceID":        record.Source.ID,
		"SourceIP":        record.Source.IP,
		"DestinationID":   record.Destination.ID,
		"DestinationIP":   record.Destination.IP,
		"DestinationPort": strconv.Itoa(int(record.Destination.Port)),
	}, map[string]interface{}{
		"ContextID": record.ContextID,
		"Counter":   record.Count,
		/*		"SourceID":        record.Source.ID,
				"SourceIP":        record.Source.IP,*/
		"SourcePort": record.Source.Port,
		"SourceType": record.Source.Type,
		/*		"DestinationID":   record.Destination.ID,
				"DestinationIP":   record.Destination.IP,*/
		/*"DestinationPort": record.Destination.Port,*/
		"DestinationType": record.Destination.Type,
		"Tags":            record.Tags,
		"Action":          record.Action,
		"DropReason":      record.DropReason,
		"PolicyID":        record.PolicyID,
	})
}
