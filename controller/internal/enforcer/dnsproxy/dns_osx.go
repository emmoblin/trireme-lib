// +build !linux darwin

package dnsproxy

import (
	"git.cloud.top/DSec/trireme-lib/collector"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/flowtracking"
	"git.cloud.top/DSec/trireme-lib/utils/cache"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/ipsetmanager"
)

// Proxy struct represents the object for dns proxy
type Proxy struct {
}

// New creates an instance of the dns proxy
func New(puFromID cache.DataStore, conntrack flowtracking.FlowClient, c collector.EventCollector, aclmanager ipsetmanager.ACLManager) *Proxy {
	return &Proxy{}
}

// ShutdownDNS shuts down the dns server for contextID
func (p *Proxy) ShutdownDNS(contextID string) {

}

// StartDNSServer starts the dns server on the port provided for contextID
func (p *Proxy) StartDNSServer(contextID, port string) error {
	return nil
}
