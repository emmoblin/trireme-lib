package remoteenforcer

import (
	"context"

	"git.cloud.top/DSec/trireme-lib/policy"

	"git.cloud.top/DSec/trireme-lib/controller/constants"
	"git.cloud.top/DSec/trireme-lib/controller/internal/enforcer"
	"git.cloud.top/DSec/trireme-lib/controller/internal/enforcer/utils/rpcwrapper"
	"git.cloud.top/DSec/trireme-lib/controller/internal/supervisor"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/ipsetmanager"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/packetprocessor"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/remoteenforcer/internal/counterclient"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/remoteenforcer/internal/debugclient"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/remoteenforcer/internal/dnsreportclient"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/remoteenforcer/internal/statsclient"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/remoteenforcer/internal/statscollector"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/remoteenforcer/internal/tokenissuer"
	"git.cloud.top/DSec/trireme-lib/controller/pkg/secrets"
	"go.uber.org/zap"
)

// RemoteEnforcer : This is the structure for maintaining state required by the
// remote enforcer.
// It is a cache of variables passed by the controller to the remote enforcer and
// other handles required by the remote enforcer to talk to the external processes
//
// Why is this public when all members are private ? For golang RPC server requirements
type RemoteEnforcer struct {
	rpcSecret       string
	rpcHandle       rpcwrapper.RPCServer
	collector       statscollector.Collector
	statsClient     statsclient.StatsClient
	debugClient     debugclient.DebugClient
	counterClient   counterclient.CounterClient
	dnsReportClient dnsreportclient.DNSReportClient
	procMountPoint  string
	enforcer        enforcer.Enforcer
	supervisor      supervisor.Supervisor
	service         packetprocessor.PacketProcessor
	secrets         secrets.Secrets
	ctx             context.Context
	cancel          context.CancelFunc
	exit            chan bool
	zapConfig       zap.Config
	logLevel        constants.LogLevel
	tokenIssuer     tokenissuer.TokenClient
	enforcerType    policy.EnforcerType
	aclmanager      ipsetmanager.ACLManager
}
